#
# Générique
#

# Module label 'ModuleDoliscanName'
ModuleDoliscanName=DoliSCAN pour Notes de Frais
ModuleDoliscanDesc=DoliSCAN est une application smartphone et un service web compagnon de Dolibarr pour vous permettre de gérer vos notes de frais
DoliScanImportDraft=Prête à être importée dans dolibarr
DoliScanImportDraftShort=Prête à être importée dans dolibarr
DoliScanImportEnabled=Importée dans Dolibarr
DoliScanImportEnabledShort=Importée
ImportToDolibarr=Importer dans Dolibarr
ImportFromDoliSCANFor=Prévisualisation de l'import de la note de frais issue de DoliSCAN
AllAccountsList = Liste des comptes utilisateurs DoliSCAN
DoliscanArea = Espace de configuration DoliSCAN

#
# Page d'administration
#
DOLISCAN_MAINACCEPT_CGV=Accepter les conditions générales de ventes et d'accès au service DoliSCAN
DOLISCAN_MAINACCOUNT_APIKEY=Clé d'accès à l'API du serveur DoliSCAN (responsable d'entreprise ou administrateur)
DOLISCAN_MAINACCOUNT_EMAIL=Adresse mail de votre compte administrateur sur DoliSCAN
DOLISCAN_MAINSERVER=Adresse du serveur DoliSCAN
DOLISCAN_ADVANCED_SERVER_SETTINGS=Paramètres avancés
DOLISCAN_MAINSERVER_CUSTOM=Adresse de votre server DoliSCAN auto-hébergé (le cas échéant)
DOLISCAN_ALLOW_HTTP=Autoriser les connexion http non sécurisées entre Dolibarr et DoliSCAN
DOLISCAN_ALLOW_HTTPTooltip=Très fortement déconseillé, les échanges seront accessibles (non sécurisés)
DOLISCAN_ALLOW_LOOPBACK=Autoriser les connexions locales entre Dolibarr et DoliSCAN
DOLISCAN_ALLOW_LOOPBACKTooltip=Utile si votre serveur DoliSCAN est hébergé sur le même serveur que votre Dolibarr
DOLISCAN_STARTDATEIMPORT=Date à partir de laquelle vous voulez importer les notes de frais de DoliSCAN
DOLISCAN_STARTDATEIMPORTTooltip=Vous permet de ne pas importer la totalité de l'historique. Date au format YYYY-MM-DD.
DOLISCAN_MULTICOMPANY=Le module multi société (multicompany) est détecté, voulez vous activer le partage du compte administrateur entre toutes les entités ? (Oui expérimental, le Non est conseillé) ?
DOLISCAN_MULTICOMPANYTooltip=Entez oui pour que ce compte soit super administrateur et gére toutes les autres entités dans DoliSCAN. Entrez non pour que chaque entité Dolibarr soit indépendante (et chaque administrateur devra alors ou pas activer le module pour son entité).
CreateDefaultSuppliersAccountsTxt=Pour vous simplifier la vie nous pouvons vous créér des comptes fournisseurs "Divers" pour l'association de ces différents frais (pratique si votre dolibarr est tout "neuf"). Si vous voulez procéder à cette création cliquez sur le lien suivant :
CreateDefaultSuppliersAccounts=Créer tous les comptes fournisseurs dans dolibarr
DoliscanSetup="Configuration du module Doliscan"
Settings=Réglages
0MultiEntity=Multi-Société
1Server=Serveur
2Admin=Administrateur
2SuppliersLinks=Liens Fournisseurs
3FeesType=Type de Frais
4PaymentsType=Moyens de paiements
5Divers=Divers
5end=Terminé - À propos
6Carbu=Carburant
7CustomRules=Règles spéciales
DoliscanConfigPageCarbu=Configuration spécifique pour le carburant et les règles fiscales Françaises. Voir la <a href="https://doc.cap-rel.fr/doliscan-dolibarr/gestion_des_regles_fiscales" target="_blank">documentation suivante pour plus de détails</a>
DoliscanSetupPageSrv=Vous êtes à la première étape de la configuration du module DoliSCAN, veuillez choisir le serveur DoliSCAN sur lequel vous voulez vous connecter.
customSelfHosted=custom (auto-hébergement)
officialBePaid=production (facturé)
testsForNoCost=tests (gratuit)
DoliscanSetupPage=Veuillez lire les informations ci-dessous avant d'aller plus loin.
DoliscanSetupInfo1=Attention, l'activation de votre compte DoliSCAN sur le serveur officiel de production entraînera une facturation conformément aux <a href="%s" target="_blank">tarifs suivants</a> à l'issu de la première période d'un mois gratuite vous permettant de vous faire une idée du service. Vous pouvez mettre fin à votre abonnement à tout moment. Tout mois entamé est du.
DoliscanSetupInfo2=Les conditions générales de ventes et d'accès au service sont accessibles <a href="%s" target="_blank">en suivant ce lien</a>. L'utilisation du serveur de démonstration DoliSCAN (Adresse du serveur DoliSCAN ci dessous https://doliscan.devtemp.fr/) ne vous engage en rien et aucune facture ne vous sera envoyée. L'utilisation de l'application est cependant impossible sur ce serveur de démonstration.
DoliscanSetupInfo3=La documentation de DoliSCAN est disponible <a href="%s" target="_blank">en ligne ici</a>.
DoliscanSetupInfo4=Si vous souhaitez auto-héberger votre propre serveur DoliSCAN, merci de prendre contact avec la société <a href="%s" target="_blank">CAP-REL https://cap-rel.fr</a>.
AcceptCGU=J'accepte les conditions générales d'utilisations du service et la tarification indiquée ci-dessus.
DoliscanSetupAccountExistMailSent=Ce compte utilisateur existe déjà sur le serveur doliscan.fr et pour nous assurer qu'il s'agit bien de la même personne un code de sécurité a été envoyé par courrier électronique.
DoliscanSetupPleaseEnterCode=Un code de sécurité d'administrateur a été envoyé par email, merci de le recopier ici :
DoliscanSetupPleaseEnterCodeCompany=Un code de sécurité d'entreprise a été envoyé par email pour vous donner les droits administrateur sur l'entreprise, merci de le recopier ici :
CheckConnectToDoliSCAN=Vérifier la connexion avec DoliSCAN
NewAccountDoliSCAN=Initialisation automatique DoliSCAN
ManualConfigDoliSCAN=Configuration manuelle
CheckConnectOK=Vérification de la connexion OK
CheckConnectTimeout=Connexion sortante impossible, votre dolibarr est-il bien connecté à internet sans filtrage de connexions sortantes ? Impossible de se connecter vers
ErrorFirstnameEmpty=Prénom introuvable, veuillez configurer le paramètre "Nom du(des) gestionnaire(s) (PDG, directeur, président...)" dans "Société/Organisation"
ErrorLastnameEmpty=Nom de famille introuvable, veuillez configurer le paramètre "Nom du(des) gestionnaire(s) (PDG, directeur, président...)" dans "Société/Organisation"
ErrorSocEmailEmpty=Adresse email vide, veuillez configurer le paramètre "Email" dans "Société/Organisation"
ErrorSocEmpty=Veuillez compléter la fiche "Société/Organisation" :
ErrorSocEmptyNOM=le nom de votre société est manquant
ErrorSocEmptyADDRESS=l'adresse postale de votre société est incomplète
ErrorSocEmptyZIP=le code postal de votre société est incomplet
ErrorSocEmptyTOWN=le nom de la ville est manquant
ErrorSocEmptyCOUNTRY=le nom du pays est manquant
ErrorSocEmptyWEB=l'adresse du site web de votre société est vide
ErrorSocEmptyTEL=le numéro de téléphone de votre société est manquant
ErrorSocEmptySIRET=le numéro SIRET est manquant
ErrorUserFirstnameEmpty=Prénom introuvable, veuillez configurer votre compte utilisateur et renseigner votre prénom
ErrorUserLastnameEmpty=Nom de famille introuvable, veuillez configurer votre compte utilisateur et renseigner votre nom
ErrorUserEmailEmpty=Adresse email vide, veuillez configurer votre compte utilisateur et renseigner votre adresse mail
PleaseStartPrevStep=Veuillez commencer par l'onglet précédent
DoliscanSetupMultiEntity=Veuillez choisir le mode de fonctionnement de DoliSCAN (Module Multi-Société détecté sur votre Dolibarr)
NoModif=Aucune modification apportée, vous pouvez continuer
MultiEntityNotSuperAdmin=Seul le super administrateur de l'entité principale peut avoir accès à cette page de configuration. Veuillez passer à l'étape suivante pour configurer ce module pour votre entité.
MultiEntityNotSuperAdminPro=Le partage des tiers étant actif seul le super administrateur de l'entité principale peut avoir accès à cette page de configuration
MultiEntityNotSuperAdminPerso=Le partage des notes de frais étant actif entre les entités seul le super administrateur de l'entité principale peut avoir accès à cette page de configuration
MultiEntityNotSuperAdminBanque=Le partage des comptes bancaires étant actif entre les entités seul le super administrateur de l'entité principale peut avoir accès à cette page de configuration
SetupErrorDateFormat=Erreur de format de date, vérifiez bien que la date est saisie sous la forme YYYY-MM-JJ, par exemple 2021-06-15 pour le 15 juin 2021
DoliscanConfigPage=Configuration du module DoliSCAN et table de correspondance entre DoliSCAN et Dolibarr...
DoliscanConfigPagePro=Veuillez configurer l'association des frais payés par un moyen de paiement professionnel (DoliSCAN) avec les fournisseurs correspondants dans Dolibarr (une facture fournisseur sera générée pour chaque frais payé avec un moyen de paiement professionnel. Nous vous conseillons de créer des fournisseurs "génériques" comme "carburant")
DoliscanConfigPagePerso=Veuillez configurer l'association des frais payés par un moyen de paiement personnel (DoliSCAN) avec les entrées correspondantes des frais Dolibarr.
DoliscanConfigPageBanque=Veuillez configurer l'association des moyens de paiments professionnels DoliSCAN avec les comptes bancaires et moyens de paiements de Dolibarr.
DoliscanConfigPageBanqueOtherCBPro=Si vous avez des collaborateurs qui utilisent des CB de l'entreprise issues d'autres banques que celle configurée vous pourrez faire une configuration différentiée ci-dessous (les comptes des utilisateurs doivent être actifs).
AccountActivatedOK=Création et Activation du compte DoliSCAN terminée. Un courrier électronique de confirmation devrait arriver d'ici peu de temps.
AccountActivatedOK2=Consultez vos email et suivez les instructions pour utiliser DoliSCAN. Puis chaque début de mois (après le 6) vous pourrez revenir dans dolibarr pour synchroniser votre compte et importer vos frais du mois précédent dans la rubrique GRH/DoliSCAN.

# Cron
AutomaticSyncDoliSCAN=Synchronisation automatique avec DoliSCAN
SyncHour=Configurez cette tâche planifiée pour qu'elle soit lancée après 16h (Pour être certain que les PDF du serveur DoliSCAN soient disponibles)

#
# Les droits
#
# (each active account is billable)
ReadMyOwnDoliSCANaccount=L'utilisateur a le droit d'accès en lecture sur son propre compte DoliSCAN
CreateMyOwnIndividualDoliSCANaccount=L'utilisateur a le droit de créer son compte individuel pour utiliser DoliSCAN (chaque compte actif est facturé)
DeleteMyOwnDoliSCANaccount=L'utilisateur a le droit de supprimer son compte DoliSCAN
ReadMyOwnDoliSCAN=L'utilisateur a le droit d'accès en lecture à ses notes de frais DoliSCAN
DeleteMyOwnDoliSCAN=L'utilisateur a le droit de supprimer ses notes de frais DoliSCAN
DownloadOthersDoliSCAN=Droits de téléchargement des DoliSCAN des autres utilisateurs de Dolibarr (administrateur pour automatiser le transfert DoliSCAN -> Dolibarr)
ReadAllDoliSCANaccounts=Droits de lecture des comptes utilisateurs de DoliSCAN (admin)
CreateAllDoliSCANaccounts=Droits de création de comptes utilisateurs dans DoliSCAN (admin)
DeleteAllDoliSCANaccounts=Droits de suppression de comptes utilisateurs de DoliSCAN (admin)

#
# Page À propos
#
About="À propos"
DoliscanAbout="À propos de DoliSCAN"
DoliscanAboutPage="Page à propos de DoliSCAN"

#
# Page de gestion du compte
#
DoliSCANMyAccount=Mon compte
MyExpenses=Liste
ActivateMyAccount=Activer mon compte DoliSCAN
AllExpenses=Tous les DoliSCAN
DoliSCANAllAccounts=Tous les comptes
DoliSCANAllAccountsList=Liste de tous les comptes utilisateurs
SyncAllUsers=Synchroniser les notes de frais de tous les utilisateurs
SyncMe=Synchroniser mes notes de frais
NoRecordFoundClickHereToCreateAccount=Votre compte n'est pas encore créé, cliquez sur le lien suivant pour l'activer:
DeleteMyAccount=Supprimer le lien avec le compte DoliSCAN ?
ConfirmDeleteMyAccountDetails=Êtes-vous sûr de vouloir supprimer le lien avec le compte DoliSCAN ? Le compte DoliSCAN ne sera pas supprimé et vous pourrez re-créer un lien entre le compte Dolibarr et DoliSCAN sans perte de données.

#
# Page d'exemple
#
MyPageName="Nom de ma page"

#
# Box d'exemple
#
MyWidget="Mon widget"
MyWidgetDescription="Description de mon widget"
ListMyAccount=Mon compte DoliSCAN
NewMyAccount=Mon compte DoliSCAN


#
# Liste des notes de frais issues de DoliSCAN
#
ListOfMyNDFS=Liste des Notes de Frais de DoliSCAN
DetailListOfMyNDFS=Vous trouverez ci-dessous la liste des Notes de Frais issues de DoliSCAN, elles ne sont pas forcément encore "ventilées" dans votre Dolibarr...


#
# Page principale du module
#
DoliSCANStats=Statistiques
DoliSCANnbAccounts=Nombre de comptes utilisateurs configurés sur DoliSCAN
DoliSCANnbNDFwaiting=Nombre de notes de frais en attente
DoliSCANnbNDFsynced=Nombre de notes de frais synchronisées
DoliSCANnbNDFlastDateSync=Date de dernière synchronisation
DoliSCANnbNDFlastDateImport=Date de dernier import d'une note de frais


DolibarrCBProForUser = Banque dans dolibarr
doliscanAccount=Compte DoliSCAN
dolibarrLogin=Identifiant Dolibarr
lastname=Nom
firstname=Prénom
emaildolibarr=Adresse mail dans Dolibarr
DOLISCAN_MYPARAM1=My param 1
DOLISCAN_MYPARAM1Tooltip=My param 1 tooltip
DOLISCAN_MYPARAM2=My param 2
DOLISCAN_MYPARAM2Tooltip=My param 2 tooltip
DoliSCANsyncProjects = Synchroniser les projets Dolibarr avec DoliSCAN
DoliSCANSyncNow = Lancer la synchronisation
DoliSCANSyncDone = Terminée
DoliScanCodeHelp = Ce code sera proposé dans l'application DoliSCAN (smartphone) pour associer un frais à ce projet
DoliScanCode = Code DoliSCAN
DoliSscanTagsPushOK = Les codes projets sont synchronisés avec le serveur DoliSCAN
DoliSscanTagsPushErr = Erreur de synchronisation avec le serveur DoliSCAN
RelatedExpenseReports = Note de frais Dolibarr
TotalDoliscanAmount = Montant total TTC DoliSCAN
TotalPersonnalAmount = Montant TTC des frais payés par un moyen personnel -> Note de frais Dolibarr
TotalProAmount = Montant total TTC des factures fournisseurs payées par un moyen de paiement de l'entreprise
PersonalAmount = Payé perso.
ProfessionalAmount = Payé pro.
ConfirmFullDeleteAccount = Suppression totale du compte ?
ConfirmFullDeleteAccountDetails = Voulez vous réellement supprimer ce compte du serveur distant DoliSCAN ? Toutes ses archives seront supprimées. Vérifiez bien de les avoir téléchargées avant de supprimer le compte ...
AccountDeleteOK = Votre compte utilisateur a été supprimé sur le serveur DoliSCAN.
AccountDeleteErr = Erreur de suppression de votre compte utilisateur sur le serveur DoliSCAN !
AccountDisableErr = Erreur de désactivation de votre compte sur le serveur DoliSCAN !
CheckConnectError = Erreur de connexion avec le serveur DoliSCAN.
CheckConnectErrorAdmin = Erreur de connexion avec le serveur DoliSCAN. Avant de refaire toute la configuration vérifiez que votre serveur a bien le droit d'initier une connexion sortante vers l'API du serveur DoliSCAN ...
tokenErrorForUser = La clé d'API pour l'utilisateur %s n'est plus utilisable, veuillez la réactualiser (menu DoliSCAN / tous les comptes, supprimer le lien puis re-créer le lien avec le compte DoliSCAN)
ThisAccountCantBeModyfied = Ce compte ne peut pas être modifié (administrateur ou compte 'racine')
ThisAccountWillNotBeDeletedOnRemote = Le compte ne sera pas supprimé sur le serveur DoliSCAN
ThisAccountWillBeCreatedOnRemote = Le compte sera créé sur le serveur DoliSCAN si nécessaire
ClicHereToCreateLink = cliquez ici pour créer le lien avec son compte DoliSCAN
ClicHereToDeleteLinkOrAccount = %scliquez ici pour supprimer le lien avec son compte DoliSCAN%s ou ici pour %ssupprimer complètement son compte DoliSCAN%s
ThisAccountWillBeDeletedOnRemote = Le compte sera supprimé du serveur DoliSCAN
tokenRenewForUser = La clé d'API pour l'utilisateur %s a été correctement actualisée. Vous pouvez lancer une nouvelle synchronisation pour récupérer les données liées à ce compte.
TheAccountIsNowReady = Ce compte a bien été créé, il est maintenant prêt à être utilisé.
ErrorCreatingAccount = La création de ce compte a échoué
GetTypeFraisPersoError = Erreur de récupération de la liste des types de frais personnels, vérifiez que le serveur doliscan est bien joignable en utilisant le bouton de test de connexion disponible sur l'onglet administrateur de la configuation du module
GetTypeFraisPersoOK = La liste des types de frais pesonnels est actualisée
GetTypeFraisPaiementProError = Erreur de récupération de la liste des types de frais professionnels, vérifiez que le serveur doliscan est bien joignable en utilisant le bouton de test de connexion disponible sur l'onglet administrateur de la configuation du module
GetTypeFraisProOK = La liste des types de frais professionnels est actualisée
GetTypeFraisProError = Erreur de récupération de la liste des types de frais professionnel, vérifiez que le serveur doliscan est bien joignable en utilisant le bouton de test de connexion disponible sur l'onglet administrateur de la configuation du module
DoliscanConfigDivers = Vous pouvez décider d'automatiser un certain nombre d'actions...
DOLISCAN_AUTO_CREATE_NEW_USER = Créer automatiquement le compte DoliSCAN lorsque vous ajoutez un utilisateur dans Dolibarr (Note: attention à la facturation éventuellement liée)
AccountActivatedErr = Erreur d'activation du compte utilisateur sur le serveur DoliSCAN, vous devrez probablement l'activer manuellement depuis le menu GRH/DoliSCAN...
ErrorEmailAddressIsNeeded = DoliSCAN : Le compte utilisateur n'a pas été créé car une adresse mail est nécessaire ... veuillez configurer le compte de cet utilisateur et activez ensuite son compte DoliSCAN
DoliSscanThreIsNoProject = Aucun projet à synchroniser, vérifiez que vos projets sont bien validés
DoliScanImportFrozen = Gelée
DoliScanBlacklistError = L'adresse IP de votre serveur semble être dans notre liste noire (ça arrive souvent si vous êtes sur un serveur mutualisé ou l'adresse IP "de sortie" de votre dolibarr est utilisée par d'autres personnes). Cliquez sur le <a target="_blank" href="https://bl.cap-rel.fr/check.php?data=%s">lien suivant</a> pour faire une demande de nettoyage de votre adresse IP (%s).
DoliScanBlacklistErrorTitle = Erreur de filtrage !
DoliScanBlacklistErrorFull = Votre serveur semble être complètement blacklisté ... aucun des serveurs CAP-REL n'est joignable, y compris le serveur sur lequel nous n'avons aucun filtrage ! Vérifiez que votre serveur peut se connecter à des ressources distantes et si besoin demandez de l'aide auprès de CAP-REL.
DoliSCANsyncDocuments = Synchroniser les pièces justificatives manquantes
DoliSCANSyncDocumentsNow = Lancer la vérification maintenant
ErrorDoliscanModuleVersionDatabase = Module DoliSCAN: Attention la version de votre module n'est pas cohérente par rapport à la version de la base de données, pensez bien à désactiver et réactiver le module ...
doliscanSyncDay = Une fois par jour (n'essayez pas de réduire ce délais le serveur n'acceptera pas vos requêtes et vous risquerez de tomber dans la blacklist)
DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES = Choix du produit / service à utiliser sur les factures fournisseurs à partir de la configuration de <a href="https://www.dolistore.com/fr/modules/1560-OCR---ScanInvoices.html">ScanInvoices (module gratuit)</a>
DOLISCAN_USE_PROJECT_LABEL = Utiliser le libellé du projet tronqué à 24 caractères si le "code doliscan" n'est pas renseigné
DOLISCAN_HIDE_CLOSED_PROJECTS = Masquer les projets clôturés même s'ils n'ont pas de date de clôture ou que celle ci n'est pas encore atteinte
DOLISCAN_REFRESH_DRAFT = Supprimer les notes de frais en brouillon avant la synchronisation

DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED = Ajouter automatiquement le paiement pour les achats réalisés avec un moyen de paiement professionnel (facture fournisseur). Attention la facture fournisseur sera automatiquement validée si vous activez cette option.
doliscanFromUser = Importé depuis DoliSCAN de %s de %s.
DOLISCAN_ENABLED_FOR_EXTERNAL_USERS = Autoriser aussi les utilisateurs externes à avoir un compte DoliSCAN
DOLISCAN_ENABLE_FRENCH_CARBU_RULES = Activer l'astuce de création des factures fournisseurs de carburant pour répondre aux règles fiscales françaises. <i>(<a href="https://doc.cap-rel.fr/doliscan-dolibarr/gestion_des_regles_fiscales" target="_blank">plus de détails</a>)</i>
DOLISCAN_ENABLE_FRENCH_CARBU_100_PERCENT = Produit à affecter dans le cas du carburant dont la totalité de la TVA est récupérable
DOLISCAN_ENABLE_FRENCH_CARBU_80_PERCENT = Produit à affecter dans le cas du carburant dont 80%% de la TVA est récupérable
DOLISCAN_ENABLE_FRENCH_CARBU_0_PERCENT = Produit à affecter à la ligne spéciale de complément lorsque la TVA est partiellement récupérable (ligne à 0%%)
DoliscanSetupCarbu = Configuration spécifique pour le carburant
GetNdfDetailsError = Erreur de récupération des détails des notes de frais, vérifiez que le serveur doliscan est bien joignable en utilisant le bouton de test de connexion disponible sur l'onglet administrateur de la configuation du module et que le module est totalement configuré

DoliscanConfigPageSpecialRules = Gestion des règles spéciales pour les frais payés par un moyen de paiement personnel
DOLISCAN_ENABLE_CUSTOM_RULES = Activer les règles spéciales (il est conseillé de faire des groupes dédiés pour éviter les risques d'appartenances multiples)
DOLISCAN_CUSTOM_RULES_RESTAURATION = Activer les règle spéciale pour les frais de restauration du <b>groupe %s</b>
DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_ALONE = Plafond de prise en charge des frais de restauration (seul)
DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_ALONETooltip = Laisser vide pour ne pas appliquer de plafond
DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_WITHCUSTOMER = Plafond de prise en charge des frais de restauration (avec invité)
DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_WITHCUSTOMERTooltip = Laisser vide pour ne pas appliquer de plafond
DOLISCAN_CUSTOM_RULES_GROUPS = Liste des groupes concernés par des règles spéciales

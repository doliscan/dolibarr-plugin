-- for mysql
ALTER TABLE llx_doliscan_myaccount CHANGE dsAPI ds_api TEXT NULL DEFAULT NULL; 
ALTER TABLE llx_doliscan_myaccount CHANGE dsLogin ds_login TEXT NULL DEFAULT NULL; 
ALTER TABLE llx_doliscan_myaccount CHANGE syncDateStart sync_date_start date NULL DEFAULT NULL; 
ALTER TABLE llx_doliscan_myaccount CHANGE syncDateStop sync_date_stop date NULL DEFAULT NULL; 


-- for postgresql
ALTER TABLE llx_doliscan_myaccount RENAME dsAPI TO ds_api;
ALTER TABLE llx_doliscan_myaccount RENAME dsLogin TO ds_login;
ALTER TABLE llx_doliscan_myaccount RENAME syncDateStart TO sync_date_start;
ALTER TABLE llx_doliscan_myaccount RENAME syncDateStop TO sync_date_stop;

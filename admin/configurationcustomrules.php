<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2023 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/admin/setup.php
 * \ingroup doliscan
 * \brief   Doliscan setup page.
 */
// require_once __DIR__ . '/../vendor/autoload.php';

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}

// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}

if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}

// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}

if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}

if (!$res) {
	die("Include of main fails");
}

// Libraries
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once DOL_DOCUMENT_ROOT . '/core/lib/functions2.lib.php';
require_once DOL_DOCUMENT_ROOT . '/compta/bank/class/account.class.php';
//require_once "../class/myclass.class.php";

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

$modDoliscan = new modDoliscan($db);

// Translations
$langs->loadLangs(array("errors", "admin", "doliscan@doliscan"));

// Access control
if (!$user->admin) {
	accessforbidden();
}

// Parameters
$action = GETPOST('action', 'aZ09');
$backtopage = GETPOST('backtopage', 'alpha');

$value = GETPOST('value', 'aZ09');

$error = 0;
$setupnotempty = 0;

$useFormSetup = 1;

if (!class_exists('FormSetup')) {
	// For retrocompatibility Dolibarr < 16.0
	// if (floatval(DOL_VERSION) < 16.0 && !class_exists('FormSetup')) {
		require_once __DIR__.'/../backport/v16/core/class/html.formsetup.class.php';
	// } else {
		// require_once DOL_DOCUMENT_ROOT.'/core/class/html.formsetup.class.php';
	// }
}

$formSetup = new FormSetup($db);
$form = new Form($db);

$formSetup->newItem('DOLISCAN_ENABLE_CUSTOM_RULES')->setAsYesNo();

$selected_groups = explode(',', dsbackport_getDolGlobalString('DOLISCAN_CUSTOM_RULES_GROUPS'));
$groups = doliscanSelect_users_groups();
foreach($groups as $groupid => $groupname) {
	if(in_array($groupid,$selected_groups)) {
	$item = $formSetup->newItem('DOLISCAN_CUSTOM_RULES_' . $groupid . '_RESTAURATION')->setAsYesNo();
	$item->nameText = $langs->trans('DOLISCAN_CUSTOM_RULES_RESTAURATION', $groupname);

	$item = $formSetup->newItem('DOLISCAN_CUSTOM_RULES_' . $groupid . '_RESTAURATION_MAX_ALONE')->setAsInteger();
	$item->nameText = $langs->trans('DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_ALONE');
	$item->helpText = $langs->trans('DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_ALONETooltip');

	$item = $formSetup->newItem('DOLISCAN_CUSTOM_RULES_' . $groupid . '_RESTAURATION_MAX_WITHCUSTOMER')->setAsInteger();
	$item->nameText = $langs->trans('DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_WITHCUSTOMER');
	$item->helpText = $langs->trans('DOLISCAN_CUSTOM_RULES_RESTAURATION_MAX_WITHCUSTOMERTooltip');
	}
}
// print json_encode($options);exit;


/*
 * Actions
 */
if ( versioncompare(explode('.', DOL_VERSION), array(15)) < 0 && $action == 'update' && !empty($user->admin)) {
	$formSetup->saveConfFromPost();
}
include DOL_DOCUMENT_ROOT.'/core/actions_setmoduleoptions.inc.php';


/*
 * View
 */

$help_url = '';
$page_name = "DoliscanSetupCustomRules";

llxHeader('', $langs->trans($page_name), $help_url);

// Subheader
$linkback = '<a href="' . ($backtopage ? $backtopage : DOL_URL_ROOT . '/admin/modules.php?restore_lastsearch_values=1') . '">' . $langs->trans("BackToModuleList") . '</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_doliscan@doliscan');

// Configuration header
$head = doliscanAdminPrepareHead();
print dol_get_fiche_head($head, 'configurationcustomrules', '', -1, "doliscan@doliscan");

// Setup page goes here
echo '<span class="opacitymedium">' . $langs->trans("DoliscanConfigPageCustomRules") . '</span><br><br>';

if ($action == 'edit') {
	print $formSetup->generateOutput(true);
} else {
	print $formSetup->generateOutput();
	print '<div class="tabsAction">';
	print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=edit&token='.newToken().'">'.$langs->trans("Modify").'</a>';
	print '</div>';
}


// Page end
print dol_get_fiche_end();

llxFooter();
$db->close();
